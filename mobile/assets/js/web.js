window.onscroll = function() {
	scrollFunction()
};

// Open Side Navigasi
function navhead(x) {
	// document.getElementById("nav-slide").style.display = "block";
	var nav = x.classList.toggle("change");

	if(nav == true){
		document.getElementById("nav-slide").style.top = "80px";
	}else{
		document.getElementById("nav-slide").style.top = "-1500px";
	}
}

// Swipper Nav
var swiper = new Swiper('.navSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.nav-next',
      prevEl: '.nav-prev',
    },
});

// Swipper Berita Populer
var swiper = new Swiper('.populerSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.terpopuler-next',
      prevEl: '.terpopuler-prev',
    },
});

// Swipper Rekomendasi
var swiper = new Swiper('.rekomendasiSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.rekomendasi-next',
      prevEl: '.rekomendasi-prev',
    },
});

// Swipper Kanal
var swiper = new Swiper('.kanalSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.slide-kanal-next',
      prevEl: '.slide-kanal-prev',
    },
	pagination: {
	  el: ".swiper-pagination",
	},
});

// Swipper Berita Terkait
var swiper = new Swiper('.beritaTerkaitSwiper', {
    slidesPerView: 'auto',
    navigation: {
      nextEl: '.berita-terkait-next',
      prevEl: '.berita-terkait-prev',
    },
});

// DatePicker
$('.datepicker').datepicker({
	format: 'dd-mm-yyyy'
});

// Back To Top
var btnTop = document.getElementById("Btn2Top");

function scrollFunction() {
	if (document.body.scrollTop > 400 || document.documentElement.scrollTop > 400) {
		btnTop.style.display = "block";
	} else {
		btnTop.style.display = "none";
	}
}

function topFunction() {
	$("html, body").animate({scrollTop: 0}, 500);
}